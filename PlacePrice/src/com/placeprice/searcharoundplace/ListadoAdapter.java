package com.placeprice.searcharoundplace;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import com.placeprice.R;

import com.placeprice.utils.Place;

/**
 * Created by peri on 13/10/13.
 */
public class ListadoAdapter extends ArrayAdapter<Place> {

    public List<Place> placeList;
    private Context context;

    public ListadoAdapter(Context context, int textViewResourceId, List<Place> list){
        super(context, textViewResourceId, list);
        placeList = list;
        this.context = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.one_item, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.listado_item_name);
        textView.setText(placeList.get(position).getName_place());

        return rowView;
    }

}
