package com.placeprice.searcharoundplace;

import android.graphics.Bitmap;


import java.util.List;

/**
 * Created by peri on 11/11/13.
 */
public interface IImageLoad {
    public void ImageTaskCallback(List<Bitmap> result);
}
