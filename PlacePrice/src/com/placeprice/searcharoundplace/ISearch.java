package com.placeprice.searcharoundplace;

import java.util.List;

import com.placeprice.utils.Place;

/**
 * Created by peri on 15/10/13.
 */
public interface ISearch {
    public void TaskCallback(List<Place> result);
}
