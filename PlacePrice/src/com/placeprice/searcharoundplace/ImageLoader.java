package com.placeprice.searcharoundplace;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by peri on 11/11/13.
 */
public class ImageLoader extends AsyncTask<String,Void,List<Bitmap>>{


    Context context;
    List<Bitmap> bitmaps;
    ProgressDialog dialog;

    public ImageLoader(Context context){
        this.context = context;
        bitmaps = new ArrayList<Bitmap>();
    }


    @Override
    protected List<Bitmap> doInBackground(String... params) {
    	for(String url : params){
            Bitmap bmp = LoadImageFromWebOperations(url);
            if(bmp != null){
            	bitmaps.add(bmp);
            }
            
        }

        return bitmaps;
    }


    public Bitmap LoadImageFromWebOperations(String url) {
        try {
            URL u = new URL(url);
            InputStream is = u.openStream();
            Bitmap bmp = BitmapFactory.decodeStream(is);
            is.close();
            return bmp;
        } catch (Exception e){e.printStackTrace();
            return null;
        }
    }

    protected void onPostExecute(List<Bitmap> result){
        super.onPostExecute(result);
        dialog.dismiss();
        ((IImageLoad)this.context).ImageTaskCallback(result);
    }


    protected  void onPreExecute(){
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setMessage("Cargando las imagenes, por favor espere...");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(true);
        dialog.show();
    }


}
