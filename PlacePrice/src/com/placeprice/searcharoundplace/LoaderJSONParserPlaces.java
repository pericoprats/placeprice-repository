package com.placeprice.searcharoundplace;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.placeprice.utils.Place;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by peri on 7/10/13.
 */
public class LoaderJSONParserPlaces extends android.os.AsyncTask<String, Void,List<Place>> {

    private List<Place> places;
    private JSONObject jObject;
    private Context context;
    private ProgressDialog dialog;
    private boolean distance = false;

    public LoaderJSONParserPlaces(Context pContext, boolean distance){
        this.context = pContext;
        this.distance = distance;
    }

    public boolean isInternetAllowed(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni!=null && ni.isAvailable() && ni.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    protected List<Place> doInBackground(String... url){
        if(url.length!= 0){
           try{
               places = readPlacesArray(url[0], distance);
           }catch (IOException e){e.printStackTrace();}
        }else{
            places = null;
        }
        return places;
    }

    public JSONObject getJsonFromUrl(String urlIn){
        HttpURLConnection httpcon;
        JSONObject json = null;
        String result = "";
        try{
            URL url = new URL(urlIn);
            httpcon = (HttpURLConnection)url.openConnection();
            httpcon.setConnectTimeout(5000);
            httpcon.connect();

            if(HttpURLConnection.HTTP_OK == httpcon.getResponseCode()){
                InputStream is = httpcon.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                StringBuilder sb = new StringBuilder();
                String line = null;
                while((line = reader.readLine())!= null){
                    sb.append(line + "\n");
                }
                result = sb.toString();
                reader.close();
                is.close();
                httpcon.disconnect();
            }else {

                httpcon.disconnect();
                Log.e("getJsonFromUrl()", httpcon.getResponseMessage());
                json = null;
                Toast.makeText(context.getApplicationContext(),"No ha sido posible realizar la conexi�n con el servidor, por favor intentalo de nuevo", Toast.LENGTH_LONG).show();
            }
        }catch (IOException e){e.printStackTrace();}
        try{
            json = new JSONObject(result);
        }catch (JSONException e){e.printStackTrace();}

        return json;
    }

    public List<Place> readPlacesArray(String url, boolean distance) throws IOException {
        jObject = getJsonFromUrl(url);
        if(jObject != null){
        	if(distance){
        		try{
                    places = readPlaceDistance(jObject.getJSONArray("places"));
                   }catch (JSONException e){e.printStackTrace();}
        	}else{
        		try{
                    places = readPlace(jObject.getJSONArray("places"));
                   }catch (JSONException e){e.printStackTrace();}
        	}
           
        }
        //Places.setListPlaces(places);
        //List<Place> placesTest = Places.getListPlaces();

        return places;
    }



    public List<Place> readPlace(JSONArray placeArray) throws IOException {
        places = new ArrayList<Place>();
        for(int i = 0; i < placeArray.length(); i++){
            try{
            Integer id = placeArray.getJSONObject(i).getInt("id");
            String name = placeArray.getJSONObject(i).getString("place");
            String city = placeArray.getJSONObject(i).getString("city");
            Double latitude = placeArray.getJSONObject(i).getDouble("latitude");
            Double longitude = placeArray.getJSONObject(i).getDouble("longitude");
            Integer numphoto = placeArray.getJSONObject(i).getInt("fotos");
            places.add(Place.createPlace(id, name,city,latitude, longitude, numphoto));
            }catch (JSONException e){e.printStackTrace();}
        }
       // places = resultAdd;
        return places;
    }
    
    public List<Place> readPlaceDistance(JSONArray placeArray) throws IOException {
        places = new ArrayList<Place>();
        for(int i = 0; i < placeArray.length(); i++){
            try{
            Integer id = placeArray.getJSONObject(i).getInt("id");
            String name = placeArray.getJSONObject(i).getString("place");
            String city = placeArray.getJSONObject(i).getString("city");
            Double latitude = placeArray.getJSONObject(i).getDouble("latitude");
            Double longitude = placeArray.getJSONObject(i).getDouble("longitude");
            Integer numphoto = placeArray.getJSONObject(i).getInt("fotos");
            Double distance = placeArray.getJSONObject(i).getDouble("distance");
            places.add(Place.createPlaceDistance(id, name,city,latitude, longitude, numphoto, distance));
            }catch (JSONException e){e.printStackTrace();}
        }
       // places = resultAdd;
        return places;
    }
    
    protected  void onPreExecute(){
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setMessage("Cargando, por favor espere...");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(true);
        dialog.show();
    }

    protected void onPostExecute(List<Place> result){
        super.onPostExecute(result);
        dialog.dismiss();
        ((ISearch)this.context).TaskCallback(result);
    }
}



