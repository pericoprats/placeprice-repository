package com.placeprice.searcharoundplace;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import com.placeprice.R;
import com.placeprice.utils.Place;

/**
 * Created by peri on 13/10/13.
 */
public class ListadoAdapterDistance extends ArrayAdapter<Place> {

    public List<Place> placeList;
    private Context context;

    public ListadoAdapterDistance(Context context, int textViewResourceId, List<Place> list){
        super(context, textViewResourceId, list);
        placeList = list;
        this.context = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_list_dist, parent, false);
        TextView textNameView = (TextView) rowView.findViewById(R.id.item_dist_name);
        textNameView.setText(placeList.get(position).getName_place());
        Double dist = placeList.get(position).getDistance() * 1000;
        TextView textDistView = (TextView) rowView.findViewById(R.id.item_dist_meters);
        textDistView.setText("a  " + String.format("%.2f", dist) + "m");
        return rowView;
    }

}
