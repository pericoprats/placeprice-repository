package com.placeprice.uploadplace;

/**
 * Created by peri on 22/10/13.
 */
public interface IUpload {
    public void UpTaskCallback(String result);
    public void UpTaskPhotoCallback(String result);
    public void UpTaskDeleteCallback(String result);
    public void UpTaskAddressCallback(String result);
    public void UpTaskCityCallback(String result);
}
