package com.placeprice.uploadplace;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import com.placeprice.utils.Place;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by peri on 20/10/13.
 */
public class UploaderPlace extends AsyncTask<String, Void, String>{


    private Context context;
    private List<NameValuePair> nameValuePairs;
    private ProgressDialog dialog;	
   
    
    public UploaderPlace(Context context){
        this.context = context;
        nameValuePairs = new ArrayList<NameValuePair>(4);
    }

    public void addNameValuePlace(Place place){
        nameValuePairs.add(new BasicNameValuePair("name_place",place.getName_place()));
        nameValuePairs.add(new BasicNameValuePair("city_place", place.getCity_place()));
        nameValuePairs.add(new BasicNameValuePair("latitude_place", place.getLatitude().toString()));
        nameValuePairs.add(new BasicNameValuePair("longitude_place", place.getLongitude().toString()));
        nameValuePairs.add(new BasicNameValuePair("numphotos", place.getNum_photo().toString()));

    }

    public boolean isInternetAllowed(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni!=null && ni.isAvailable() && ni.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    public void addNameValue(String name, String value){
        nameValuePairs.add(new BasicNameValuePair(name,value));
    }

    public List<NameValuePair> getNameValuePairs(){
        return nameValuePairs;
    }

    @Override
    protected String doInBackground(String... params) {
        String result;
        if(params.length!=0){
            result = executeHttpPost(params[0]);
        }else
            result = null;
        return result;
    }


    public String executeHttpPost(String urlService){
    	HttpParams httpParameters = new BasicHttpParams();
        HttpPost httpPost =  new HttpPost(urlService);
        int timeoutConnection = 3000;
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
        int timeoutSocket = 5000;
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
        HttpClient httpClient = new DefaultHttpClient(httpParameters);
        try {
        List<NameValuePair> valuePairs = getNameValuePairs();
        httpPost.setEntity(new UrlEncodedFormEntity(valuePairs));
            try{
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            InputStream inputStream = entity.getContent();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            StringBuilder stringBuilder = new StringBuilder();
      
            //Cambios para poder usar el servidor en internet
            int c;
            while((c = bufferedReader.read()) != '\n'){
                char res = (char) c;
            	stringBuilder.append(res);
            	 
             }
            
            /* 
            while((bufferedStrChunk = bufferedReader.readLine()) != null){
                    stringBuilder.append(bufferedStrChunk);
            }*/
             Log.i("Connection made", response.getStatusLine().toString());
             return stringBuilder.toString();



            }catch (ClientProtocolException cpe){System.out.println("First Exception caz of HttpResponese :" + cpe);
                cpe.printStackTrace();Toast.makeText(context, "No se ha podido conectar, ClientProtocol", Toast.LENGTH_LONG).show();
            }catch (IOException ioe){
            	//System.out.println("Second Exception caz of HttpResponse :" + ioe);
            	//ioe.printStackTrace(); 
            	return null;
            }
    }catch(UnsupportedEncodingException uee){System.out.println("An Exception given because of UrlEncodedFormEntity argument :" + uee);
        uee.printStackTrace();Toast.makeText(context, "No se ha podido conectar, UnsupportedEncodingException", Toast.LENGTH_LONG).show();}
        return null;

    }
    
    protected  void onPreExecute(){
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setMessage("A�adiendo los datos del lugar, por favor espere...");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(true);
        dialog.show();
    }

    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        dialog.dismiss();
        ((IUpload) this.context).UpTaskCallback(result);

    }


}
