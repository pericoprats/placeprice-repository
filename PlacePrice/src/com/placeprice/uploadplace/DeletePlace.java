package com.placeprice.uploadplace;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class DeletePlace extends AsyncTask<String, Void, String>{

	
	private Context context;
    private List<NameValuePair> nameValuePairs;
    private ProgressDialog dialog;	
	
	public DeletePlace(Context context){
		
		this.context = context;
		nameValuePairs = new ArrayList<NameValuePair>(1);
	}
	
	public void addNameValuePlace(String id){   nameValuePairs.add(new BasicNameValuePair("id_place", id)); }
	
	public List<NameValuePair> getNameValuePairs(){    return nameValuePairs;}
	
	@Override
	protected String doInBackground(String... params) {
        String result;
        if(params.length!=0){
            result = executeHttpPostDelete(params[0]);
        }else
            result = null;
        return result;
    }
	
	
	public String executeHttpPostDelete(String urlService){
    	HttpParams httpParameters = new BasicHttpParams();
        HttpPost httpPost =  new HttpPost(urlService);
        int timeoutConnection = 3000;
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
        int timeoutSocket = 5000;
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
        HttpClient httpClient = new DefaultHttpClient(httpParameters);
        try {
        List<NameValuePair> valuePairs = getNameValuePairs();
        httpPost.setEntity(new UrlEncodedFormEntity(valuePairs));
            try{
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity entity = response.getEntity();
            InputStream inputStream = entity.getContent();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            StringBuilder stringBuilder = new StringBuilder();
      
            //Cambios para poder usar el servidor en internet
            int c;
            while((c = bufferedReader.read()) != '\n'){
                char res = (char) c;
            	stringBuilder.append(res);
            	 
             }
             

             Log.i("Connection made", response.getStatusLine().toString());
             return stringBuilder.toString();



            }catch (ClientProtocolException cpe){System.out.println("First Exception caz of HttpResponese :" + cpe);
                cpe.printStackTrace();Toast.makeText(context, "No se ha podido conectar, ClientProtocol", Toast.LENGTH_LONG).show();
            }catch (IOException ioe){
            	//System.out.println("Second Exception caz of HttpResponse :" + ioe);
            	//ioe.printStackTrace(); 
            	return null;
            }
    }catch(UnsupportedEncodingException uee){System.out.println("An Exception given because of UrlEncodedFormEntity argument :" + uee);
        uee.printStackTrace();Toast.makeText(context, "No se ha podido conectar, UnsupportedEncodingException", Toast.LENGTH_LONG).show();}
        return null;

    }
    
    protected  void onPreExecute(){
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setMessage("Eliminando la información de el lugar, por favor espere...");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(true);
        dialog.show();
    }
    
    protected void onPostExecute(String result){
    	super.onPostExecute(result);
    	dialog.dismiss();
    	((IUpload)this.context).UpTaskDeleteCallback(result);
    }

}
