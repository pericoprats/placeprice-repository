package com.placeprice.uploadplace;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


/**
 * Created by peri on 24/10/13.
 */
public class UploaderPhoto extends AsyncTask<String,Void, String>{

    private Context context;
    private ProgressDialog dialog;


    public UploaderPhoto(Context context){
        this.context = context;

    }


    protected String doInBackground(String... params) {
        String result = "false";
        for(String path : params){
           result = upLoad(path);
           if(!result.equals("true")){
               result = "false";
               break;
           }


        }
        //String result = upLoad(myPhotoPath);

        return result;
    }

    private String upLoad(String path){
         try{
             HttpClient httpClient = new DefaultHttpClient();
             httpClient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
             HttpPost httpPost = new HttpPost("http://www.placeprice.bugs3.com/servicephp/service.add2photo.php");
             File file = new File(path);
             MultipartEntity mpEntity = new MultipartEntity();
             ContentBody photo = new FileBody(file, "image/jpeg");
             mpEntity.addPart("photoup", photo);
             httpPost.setEntity(mpEntity);
             HttpResponse response = httpClient.execute(httpPost);
             httpClient.getConnectionManager().shutdown();
             HttpEntity entity = response.getEntity();
             InputStream inputStream = entity.getContent();
             InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
             BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

             StringBuilder stringBuilder = new StringBuilder();

             int c;
             while((c = bufferedReader.read()) != '<'){
                 char res = (char) c;
             	stringBuilder.append(res);
             	 
              }
             stringBuilder.toString();
             return stringBuilder.toString();
         }catch (IOException e){e.printStackTrace();}
         return null;

    }

    protected  void onPreExecute(){
        super.onPreExecute();
        dialog = new ProgressDialog(context);
        dialog.setMessage("A�adiendo las im�genes del lugar, por favor espere...");
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCancelable(true);
        dialog.show();
    }

    protected void onPostExecute(String result){
        super.onPostExecute(result);
        dialog.dismiss();
        ((IUpload)this.context).UpTaskPhotoCallback(result);
    }

}
