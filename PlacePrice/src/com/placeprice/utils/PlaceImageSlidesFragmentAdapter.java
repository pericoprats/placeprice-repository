package com.placeprice.utils;

import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;


/**
 * Created by peri on 30/10/13.
 */
public class PlaceImageSlidesFragmentAdapter extends FragmentPagerAdapter{

    private Bitmap[] Images;//TODO

   // protected static final int[] ICONS = new int[]{}; //TODO

    private int mCount;

    public PlaceImageSlidesFragmentAdapter(FragmentManager fm, List<Bitmap> bitmaps) {
        super(fm);
        Images = new Bitmap[bitmaps.size()];
        for(int i = 0; i < bitmaps.size(); i++){
            Images[i] = bitmaps.get(i);
        }
        mCount = Images.length;
    }

    @Override
    public Fragment getItem(int position) {
        return new PlaceImageSlidesFragment(Images[position]);
    }

  /*  @Override
    public int getIconResId(int index) {
        return ICONS[index % ICONS.length];
    }*/

    @Override
    public int getCount() {
        return mCount;
    }



}
