package com.placeprice.utils;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.placeprice.uploadplace.IUpload;

/**
 * Created by peri on 23/10/13.
 */
public class GPSTracker  extends Service implements LocationListener{

    private final Context mContext;

    // flag for GPS status
    boolean isGPSEnabled = false;

    public Geocoder geocoder;

    // flag for network status
    boolean isNetworkEnabled = false;

    boolean canGetLocation = false;

    Location location; // location
    double latitude; // latitude
    double longitude; // longitude
    
    
    
    List<Address> addressGeo;

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1 / 4; // 25 seconds
    
    // Declaring a Location Manager
    protected LocationManager locationManager;
    
    public GPSTracker(Context context) {
        this.mContext = context;
        geocoder = new Geocoder(context, Locale.getDefault());
        addressGeo = new ArrayList<Address>();
        location = getLocation();
    }


    public Location getLocation() {
        try {
            locationManager = (LocationManager) mContext
                    .getSystemService(LOCATION_SERVICE);

            // getting GPS status
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            //isNetworkEnabled
            if(!isGPSEnabled && ! isNetworkEnabled) {
                showSettingsAlert().show();
         
                // no network provider is enabled
            } else {
                this.canGetLocation = true;
     
                // First get location from Network Provider
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
             // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location != null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }
    
    @Override
    public void onLocationChanged(Location newLocation) {
    	if(isBetterLocation(newLocation, location)){
    		this.location = newLocation;
    	}
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onProviderDisabled(String provider) {}

    @Override
    public IBinder onBind(Intent intent) {return null;}

    public void stopUsingGPS(){
        if(locationManager != null){
            locationManager.removeUpdates(GPSTracker.this);
        }
    }
    
    public Location getBestLocation(){
    	if(location != null){
    		if(isBetterLocation(getLocation(), location)){
    			return getLocation();
    		}else{
    			return location;
    		}
    	}else{
    		return getLocation();
    	}
    }

    public double getLatitude(){
        if(location != null){
            latitude = location.getLatitude();
        }
        return latitude;
    }

    public double getLongitude(){
        if(location != null){
            longitude = location.getLongitude();
        }
        return longitude;
    }
    
    public void executeGeocoder(Context context){
    	GetGeocoderInfo getGeocoderInfo = new GetGeocoderInfo(context);
    	getGeocoderInfo.execute();
    }

    public boolean canGetLocation() {
        return this.canGetLocation;
    }


    public String getAddress(){
        String address = "";
        address = addressGeo.get(0).getAddressLine(0);
        
        return address;
    }

    public String getCity(){
       String city = "";
       city = addressGeo.get(0).getAddressLine(1);
       city.split(" , ");
       return city;
    }
    
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > MIN_TIME_BW_UPDATES;
        boolean isSignificantlyOlder = timeDelta < -MIN_TIME_BW_UPDATES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than 25 seconds since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
        // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
          return provider2 == null;
        }
        return provider1.equals(provider2);
    }
    

    public AlertDialog showSettingsAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("El GPS no est� habilitado. �Quieres ir al menu de configuraci�n de GPS?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        return alertDialog.create();
    }


    private class GetGeocoderInfo extends AsyncTask<Void, Void, List<Address>>{
    	
    	Context context;
    	public GetGeocoderInfo(Context context){
    		this.context = context;
    	}
		@Override
		protected List<Address> doInBackground(Void... params) {
			List<Address> addResult = new ArrayList<Address>();
			if(params != null){
				try{
					addResult = getAddressesFromGeo();
				}catch(IOException e){
					e.printStackTrace();}
				
			}else{
				return null;
			}
			return addResult;
		}
		
		public List<Address> getAddressesFromGeo() throws IOException{
	        return geocoder.getFromLocation(latitude, longitude, 1);
	    }
		
		protected void onPostExecute(List<Address> result){
			super.onPostExecute(result);
			addressGeo = result;
			String address = null;
			String city = null;
			if(!addressGeo.isEmpty()){
				address = addressGeo.get(0).getAddressLine(0);
				String[] cityWithCode = addressGeo.get(0).getAddressLine(1).split(" ");
				if(cityWithCode.length != 0){
					StringBuilder cityBuilder = new StringBuilder();
					for(int i = 1 ; i < cityWithCode.length ; i++){
						cityBuilder.append(cityWithCode[i] + " ");
					}
					city = cityBuilder.toString();
				}
			}
			if(address == null || city == null){
				Toast.makeText(context, "No se ha podido obtener la localizaci�n, intentalo m�s tarde", Toast.LENGTH_LONG).show();
			}
			
			((IUpload)this.context).UpTaskAddressCallback(address);
			((IUpload)this.context).UpTaskCityCallback(city);
		}
    	
    }
    
    
}
