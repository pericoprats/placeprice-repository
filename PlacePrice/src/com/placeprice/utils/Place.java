package com.placeprice.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by peri on 8/10/13.
 */

public class Place {

    private Integer id_place;
    private String name_place;
    private String city_place;
    private Double latitude;
    private Double longitude;
    private Integer num_photo;
    private Double distance;


  public Place(Integer id_place, String name_place, String city_place, Double latitude, Double longitude, Integer num_photos){
      this.id_place = id_place;
      this.name_place = name_place;
      this.city_place = city_place;
      this.latitude = latitude;
      this.longitude = longitude;
      this.num_photo = num_photos;
  }

    public Place(String name_place, String city_place, Double latitude, Double longitude, Integer num_photos){
        this.name_place = name_place;
        this.city_place = city_place;
        this.latitude = latitude;
        this.longitude = longitude;
        this.num_photo = num_photos;
    }
    
    public Place(Integer id_place, String name_place, String city_place, Double latitude, Double longitude, Integer num_photos, Double distance){
        this.id_place = id_place;
        this.name_place = name_place;
        this.city_place = city_place;
        this.latitude = latitude;
        this.longitude = longitude;
        this.num_photo = num_photos;
        this.distance = distance;
    }


    public static Place createPlaceDistance(Integer id_place, String name_place, String city_place, Double latitude, Double longitude, Integer num_photos, Double distance) {
        return new Place(id_place, name_place, city_place, latitude, longitude, num_photos, distance);
    }

    public static Place createPlace(Integer id_place, String name_place, String city_place, Double latitude, Double longitude, Integer num_photos) {
        return new Place(id_place, name_place, city_place, latitude, longitude, num_photos);
    }

    public Integer getId_place() {
        return id_place;
    }

    public void setId_place(Integer id_place) {
        this.id_place = id_place;
    }

    public String getName_place() {
        return name_place;
    }

    public void setName_place(String name_place) {
        this.name_place = name_place;
    }

    public String getCity_place() {
        return city_place;
    }

    public void setCity_place(String city_place) {
        this.city_place = city_place;
    }

    public Double getLatitude(){ return latitude;}

    public void setLatitude(Double latitude) {  this.latitude = latitude;}

    public Double getLongitude(){ return longitude;}

    public void setLongitude(Double longitude){ this.longitude = longitude;}


    public Integer getNum_photo() {
        return num_photo;
    }

    public void setNum_photo(Integer num_photo) {
        this.num_photo = num_photo;
    }
    public Double getDistance(){
    	return distance;
    }
	
	public static List<Integer> getIds(List<Place> result){
        List<Integer> ids = new ArrayList<Integer>();
        for(Place p : result){
            ids.add(p.getId_place());
        }
        return ids;
    }

    public static List<Integer> getNumphotos(List<Place> result){
        List<Integer> nps = new ArrayList<Integer>();
        for(Place p : result){
            nps.add(p.getNum_photo());
        }
        return nps;
    }
    
}
