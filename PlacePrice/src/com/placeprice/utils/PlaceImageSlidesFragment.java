package com.placeprice.utils;

import com.polites.android.GestureImageView;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;


/**
 * Created by peri on 30/10/13.
 */

@SuppressLint("ValidFragment")
public class PlaceImageSlidesFragment extends Fragment{

    int imageResourceId;
    Bitmap imageBitmap;
    

	public PlaceImageSlidesFragment(Bitmap bmp) {
      // imageResourceId = i;
        imageBitmap = bmp;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setLayoutParams(new ViewPager.LayoutParams());
        
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

        GestureImageView view = new GestureImageView(getActivity());
        view.setImageBitmap(imageBitmap);
        view.setLayoutParams(params);
        layout.setGravity(Gravity.CENTER);
        layout.addView(view);

        return layout;
    }
}

/*
 *		ImageView image = new ImageView(getActivity());
      //image.setImageResource(imageResourceId);
        image.setImageBitmap(imageBitmap);

        
        LinearLayout layout = new LinearLayout(getActivity());
        layout.setLayoutParams(new ViewPager.LayoutParams());

        layout.setGravity(Gravity.CENTER);
        layout.addView(image);

        return layout;
        ********************************************************************
        LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        GestureImageView view = new GestureImageView(this);
        view.setImageResource(R.drawable.image);
        view.setLayoutParams(params);

        ViewGroup layout = (ViewGroup) findViewById(R.id.layout);

        layout.addView(view);
       
        
 */

