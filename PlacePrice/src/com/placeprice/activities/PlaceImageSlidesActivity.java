package com.placeprice.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.placeprice.R;
import com.placeprice.searcharoundplace.IImageLoad;
import com.placeprice.searcharoundplace.ImageLoader;
import com.placeprice.utils.PlaceImageSlidesFragmentAdapter;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.List;

/**
 * Created by peri on 2/11/13.
 */
public class PlaceImageSlidesActivity extends SherlockFragmentActivity implements IImageLoad{

    
    PlaceImageSlidesFragmentAdapter mAdapter;
    ViewPager mPager;
    CirclePageIndicator mIndicator;
    private Integer id;
    private Integer numphoto;

    protected void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        //setContentView(View.inflate(Pla));
        setContentView(R.layout.slice_images_sherlock);
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            id = extras.getInt("id");
            numphoto = extras.getInt("numphoto");
        }

       // String[] url = new String[]{"http://192.168.1.132/phpmyadmin/imagesplaces/pic_96-1.jpg","http://192.168.1.132/phpmyadmin/imagesplaces/pic_96-2.jpg","http://192.168.1.132/phpmyadmin/imagesplaces/pic_96-3.jpg"};

        ImageLoader imageLoader = new ImageLoader(this);
        imageLoader.execute(getUrlImages(id, numphoto));




       // mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
       // mIndicator.setViewPager(mPager);


    }

    public String[] getUrlImages(Integer id, Integer numphoto){
        String[] urls = new String[numphoto];
        int np = 1;
        for(int i = 0; i < numphoto ; i++){
            StringBuilder url = new StringBuilder("http://www.placeprice.bugs3.com/servicephp/imagesplaces/pic_" + id + "-" + np + ".jpg");
            urls[i] = url.toString();
            np++;
        }
        return urls;
    }


    @Override
    public void ImageTaskCallback(List<Bitmap> result) {
        if(!result.isEmpty()){
        	mAdapter = new PlaceImageSlidesFragmentAdapter(getSupportFragmentManager(), result);

            mPager = (ViewPager)findViewById(R.id.pager);
            mPager.setAdapter(mAdapter);
           
         

            mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
            mIndicator.setViewPager(mPager);
            

        }else{
        	Toast.makeText(getApplicationContext(), "Lo siento este lugar, no tiene ninguna imagen debe haber alg�n error", Toast.LENGTH_LONG).show();
        	super.onBackPressed();
        }
    	

    }
}
