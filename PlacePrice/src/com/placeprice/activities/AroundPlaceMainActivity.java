package com.placeprice.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.placeprice.R;


public class AroundPlaceMainActivity extends SherlockFragmentActivity{
	
	
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.around_main_place);
		
		Button btn = (Button) findViewById(R.id.btnGetLocation);
		btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				EditText rad = (EditText) findViewById(R.id.editTxtRadio);
				Double radio = Double.parseDouble(rad.getText().toString());
				if(radio > 0){
					Intent intent = new Intent(AroundPlaceMainActivity.this, AroundPlaceActivity.class);
					intent.putExtra("radio", radio);
					startActivity(intent);
				}else{
					Toast.makeText(getApplicationContext(), "Tienes que introducir una distancia mayor que cero", Toast.LENGTH_LONG).show();
				}
				
				
			}
		});
	}
}
