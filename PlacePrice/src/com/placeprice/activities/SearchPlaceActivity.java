package com.placeprice.activities;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;

import android.view.View;

import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.placeprice.R;
import com.placeprice.searcharoundplace.ISearch;
import com.placeprice.searcharoundplace.ListadoAdapter;
import com.placeprice.searcharoundplace.LoaderJSONParserPlaces;
import com.placeprice.utils.Place;

import java.net.URLEncoder;
import java.util.List;

/**
 * Created by peri on 9/10/13.
 */
public class  SearchPlaceActivity extends Activity implements ISearch{
/*
    private EditText namePlace;
    private EditText cityPlace;
    private List<Place> places;
    private ArrayList<String> namesPlaces;
*/
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.buscar_lugar);
        Button btnSearch = (Button)findViewById(R.id.buttonSearch);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               //hide keyboard
            	InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
               //send query
                sendQuery(view);
            }
        });
        
        
        
    }

    public void sendQuery(View v){

        LoaderJSONParserPlaces jp = new LoaderJSONParserPlaces(this, false);
        ListView lview = (ListView)findViewById(R.id.lista_en_search);
        lview.getEmptyView();
        StringBuilder sb = new StringBuilder();
        sb.append("http://www.placeprice.bugs3.com/servicephp/service.places.php");

        try{
            EditText editText = (EditText)findViewById(R.id.textSearchName);
            String nameText = editText.getText().toString();
            //nameText = nameText.trim();
            editText = (EditText)findViewById(R.id.textSearchCity);
            String cityText = editText.getText().toString();
            //cityText = cityText.trim();
            if(!cityText.isEmpty()){
                sb.append("?");
                if(!cityText.isEmpty()){
                    sb.append("c=");
                    sb.append(URLEncoder.encode(cityText, "UTF-8").toLowerCase());
                    //sb.append("'");
	                if(!nameText.isEmpty()){
	                    sb.append("&n=");
	                    sb.append(URLEncoder.encode(nameText, "UTF-8").toLowerCase());
	                  //  sb.append("'");
	                }    
                }/*else if(!cityText.isEmpty()){
                    sb.append("c='");
                    sb.append(URLEncoder.encode(cityText, "UTF-8").toLowerCase());
                    sb.append("'");
                }*/
                if(jp.isInternetAllowed(this)){
                    jp.execute(sb.toString());
                }else{
                    Toast.makeText(getApplicationContext(),"Comprueba que tienes conexi�n a Internet",Toast.LENGTH_LONG).show();
                }
                
            }else{
            	Toast.makeText(getApplicationContext(), "Tienes que introducir al menos el nombre de la ciudad", Toast.LENGTH_LONG).show();
            }
        }catch (Exception e){e.printStackTrace();}
    }
    
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
      //ignore orientation change
      super.onConfigurationChanged(newConfig);
    }

    public void TaskCallback(final List<Place> result){
        if(result != null){
            if(0 != result.size()){
               // ((Places)getApplicationContext()).lPlaces = result;
                ListView l = (ListView)findViewById(R.id.lista_en_search);
                ListadoAdapter lad = new ListadoAdapter(this, R.layout.one_item, result);
                l.setAdapter(lad);
                final ListView lview = (ListView)findViewById(R.id.lista_en_search);


                lview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        Intent intent = new Intent(SearchPlaceActivity.this, PlaceImageSlidesActivity.class);
                        intent.putExtra("id", Place.getIds(result).get(position));
                        intent.putExtra("numphoto", Place.getNumphotos(result).get(position));
                        startActivity(intent);

                    }
                });
            }else {
                Toast.makeText(getApplicationContext(), "La busqueda no delvovi� ning�n resultado", Toast.LENGTH_LONG).show();
            }
        }else {
            Toast.makeText(getApplicationContext(), "No fue posible recuperar ning�n resultado, es un problema de el servidor", Toast.LENGTH_LONG).show();
        }
    }

/*
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
*/
    








}


/*
 protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.buscar_lugar);
        namePlace = (EditText)findViewById(R.id.textName);
        cityPlace = (EditText)findViewById(R.id.textCity);
        final String nameP = namePlace.toString();
        final String cityP = cityPlace.toString();
        final StringBuilder sb = new StringBuilder("http://192.168.1.131/phpmyadmin/service.places.php?n=");
        Button btn = (Button)findViewById(R.id.buttonSearch);
        final LoaderJSONParserPlaces reader = new LoaderJSONParserPlaces(this);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              try{
                sb.append(URLEncoder.encode(nameP,"UTF-8"));
                sb.append("&c=");
                sb.append(URLEncoder.encode(cityP,"UTF-8"));
               // sb.append("'");
               }catch (IOException e){}
                try {
                places = reader.readAndParseJSONPlaces(sb.toString());
                namesPlaces = new ArrayList<String>();
                for(Place p : places){
                    namesPlaces.add(p.getName_place());
                }

               }catch (IOException e){ }
                Intent intent = new Intent(SearchPlaceActivity.this, ResultSearchPlaceActivity.class);

                //b.putStringArrayList("names", namesPlaces);
                intent.putExtra("name",namesPlaces.get(0));
                startActivity(intent);

            }
        });


    }
 */