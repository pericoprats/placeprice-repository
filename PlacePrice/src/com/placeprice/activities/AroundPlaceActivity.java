package com.placeprice.activities;

import java.util.ArrayList;
import java.util.List;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;



import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.placeprice.R;
import com.placeprice.searcharoundplace.ISearch;
import com.placeprice.searcharoundplace.ListadoAdapterDistance;
import com.placeprice.searcharoundplace.LoaderJSONParserPlaces;
import com.placeprice.utils.GPSTracker;
import com.placeprice.utils.Place;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;


/**
 * Created by peri on 14/10/13.
 */
public class AroundPlaceActivity extends SherlockFragmentActivity implements ISearch{

	private GPSTracker gps;
	private Double latitude, longitude;
	private GoogleMap map;
	private List<String> idsMarkers;
	private List<Place> placesResult;
	private Double radio;
	
	
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.placeprice.R.layout.around_place);  
        map = ((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        
        placesResult = new ArrayList<Place>();
        
        gps = new GPSTracker(this);
        
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            radio = extras.getDouble("radio");
            
        }
        
        if(sendQueryLocationPlaces()){
			map.clear();
			showMark(latitude, longitude, "Mi posici�n","Punto de partida");
			CameraPosition zoom =
					new CameraPosition.Builder()
					.target(new LatLng(latitude, longitude))
					.zoom(14.5F) // zoom
					.tilt(50F) // viewing angle
					.build();
			CircleOptions circleOptions = new CircleOptions()
			  .center( new LatLng(latitude, longitude) )
			  .radius(radio * 1000)
			  .fillColor(0x40ff0000)
			  .strokeColor(Color.TRANSPARENT)
			  .strokeWidth(2);
			map.animateCamera(CameraUpdateFactory.newCameraPosition(zoom));
			map.addCircle(circleOptions);
		}
      /*  
        Button btn = (Button)findViewById(R.id.btnPosAround);
        btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(sendQueryLocationPlaces()){
					map.clear();
					showMark(latitude, longitude, "Mi posici�n","Punto de partida");
					CameraPosition zoom =
							new CameraPosition.Builder()
							.target(new LatLng(latitude, longitude))
							.zoom(14.5F) // zoom
							.tilt(50F) // viewing angle
							.build();
					CircleOptions circleOptions = new CircleOptions()
					  .center( new LatLng(latitude, longitude) )
					  .radius(1000)
					  .fillColor(0x40ff0000)
					  .strokeColor(Color.TRANSPARENT)
					  .strokeWidth(2);
					map.animateCamera(CameraUpdateFactory.newCameraPosition(zoom));
					map.addCircle(circleOptions);
				}
				
			}
		});*/
     /*   
        ActionBar ab = getActionBar();
        ab.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        
        ActionBar.Tab map = ab.newTab().setText("MapPlace");
        ActionBar.Tab list = ab.newTab().setText("ListPlace");
        
        Fragment mapFragment = new MapPlaceFragment();
        Fragment listFragment = new ListPlaceFragment();
        
        map.setTabListener(new MyTabListener(mapFragment));
        list.setTabListener(new MyTabListener(listFragment));
        
        
        ab.addTab(map);
        ab.addTab(list);
       */ 
    }

	public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.menu_around, menu);
        return true;
      } 
	 public boolean onOptionsItemSelected(MenuItem item) {
	    	switch(item.getItemId()){
	    		case R.id.action_position:
	    			if(sendQueryLocationPlaces()){
	    				map.clear();
	    				showMark(latitude, longitude, "Mi posici�n","Punto de partida");
	    				CameraPosition zoom =
	    						new CameraPosition.Builder()
	    						.target(new LatLng(latitude, longitude))
	    						.zoom(14.5F) // zoom
	    						.tilt(50F) // viewing angle
	    						.build();
	    				CircleOptions circleOptions = new CircleOptions()
	    				  .center( new LatLng(latitude, longitude) )
	    				  .radius(radio * 1000)
	    				  .fillColor(0x40ff0000)
	    				  .strokeColor(Color.TRANSPARENT)
	    				  .strokeWidth(2);
	    				map.animateCamera(CameraUpdateFactory.newCameraPosition(zoom));
	    				map.addCircle(circleOptions);
	    			}
	    			break;
	    			
	    	}	
	    return true;
	  }

	 
	 public boolean sendQueryLocationPlaces(){
		 boolean res = false;
		 if(isWifiOn()){       
		 	if(gps.canGetLocation()){
		 			gps.getBestLocation();
		            latitude = gps.getLatitude();
		            longitude = gps.getLongitude();
		            LoaderJSONParserPlaces jp = new LoaderJSONParserPlaces(this, true);
			        ListView lview = (ListView)findViewById(R.id.listPlaceAround);
			        lview.getEmptyView();
			        StringBuilder sb = new StringBuilder();
			        sb.append("http://www.placeprice.bugs3.com/servicephp/service.location.places.php?lat='" + latitude + "'&long='" + longitude + "'&dist='" + radio + "'");
			        if(jp.isInternetAllowed(this)){
		                jp.execute(sb.toString());
		                res = true;
		            }else{
		                Toast.makeText(getApplicationContext(),"Comprueba que tienes conexi�n a Internet",Toast.LENGTH_LONG).show();
		                res = false;
		                
		            }
			        
		        }else{
		            gps.showSettingsAlert();
		            res = false;
		        }
		 }
		 return res;
	 }
	 
	public Marker showMark(double lat, double longit, String name, String snip){
		return map.addMarker(new MarkerOptions()
		.position(new LatLng(lat, longit))
        .title(name)
        .snippet(snip));
	}
	
	public void showMarksAround(List<Place> places){
		idsMarkers = new ArrayList<String>();
		placesResult = places;
		for(Place place : places){
			String dist = String.format("%.2f", place.getDistance()*1000);
			Marker mark = showMark(place.getLatitude(), place.getLongitude(), place.getName_place(), "a " + dist + " metros");
			//mark.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
			mark.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.placeprice));
			idsMarkers.add(mark.getId());
			
		}
	}
	 public Boolean isWifiOn(){
	     WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
	     int state = wifi.getWifiState();
	     if(state == WifiManager.WIFI_STATE_DISABLED || state == WifiManager.WIFI_STATE_DISABLING){
	    	 showSettingsWiFiAlert().show();
	    	 return false;
	     }else
	    	 return true;
	 }
	 
	 
	    public AlertDialog showSettingsWiFiAlert(){
	        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

	        // Setting Dialog Title
	        alertDialog.setTitle("WiFi settings");

	        // Setting Dialog Message
	        alertDialog.setMessage("El WiFi no est� habilitado.\n" + "Si tienes simplemente el Wi-Fi encendido, tendr�s una mayor exactitud en tu localici�n.\n" + "�Quieres ir al menu de configuraci�n WiFi ?");

	        // On pressing Settings button
	        alertDialog.setPositiveButton("Ir a ajustes de WiFi", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog,int which) {
	                Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
	                startActivity(intent);
	            }
	        });

	        // on pressing cancel button
	        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int which) {
	                dialog.cancel();
	            }
	        });

	        // Showing Alert Message
	        return alertDialog.create();
	    }
	 

	@Override
	public void TaskCallback(final List<Place> result){
		if(result != null){
			if(0 != result.size()){
				ListView listDistPlace = (ListView)findViewById(R.id.listPlaceAround);
				ListadoAdapterDistance lad = new ListadoAdapterDistance(this, R.layout.item_list_dist, result);
				listDistPlace.setAdapter(lad);
				showMarksAround(result);
				listDistPlace.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        gps.stopUsingGPS();
                    	Intent intent = new Intent(AroundPlaceActivity.this, PlaceImageSlidesActivity.class);
                        intent.putExtra("id", Place.getIds(result).get(position));
                        intent.putExtra("numphoto", Place.getNumphotos(result).get(position));
                        startActivity(intent);

                    }
                });
				
				map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
					
					@Override
					public void onInfoWindowClick(Marker arg0) {
						if(!arg0.getTitle().equals("Mi posici�n")){
							gps.stopUsingGPS();
							Place place = placesResult.get(idsMarkers.indexOf(arg0.getId()));
							Intent intent = new Intent(AroundPlaceActivity.this, PlaceImageSlidesActivity.class);
		                    intent.putExtra("id", place.getId_place());
		                    intent.putExtra("numphoto", place.getNum_photo());
		                    startActivity(intent);
						}else
							Toast.makeText(getApplicationContext(), "Este marker es tu posici�n actual", Toast.LENGTH_LONG).show();
						
					}
				});
				
			}else{
				Toast.makeText(getApplicationContext(), "No tienes ning�n lugar alrededor...", Toast.LENGTH_LONG).show();
			}
		}else {
            Toast.makeText(getApplicationContext(), "No fue posible recuperar ning�n resultado, es un problema de el servidor", Toast.LENGTH_LONG).show();
        }
	}

}
