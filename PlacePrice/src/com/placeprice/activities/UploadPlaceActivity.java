package com.placeprice.activities;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;

import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;

import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.view.ViewPager;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;


import android.widget.TextView;
import android.widget.Toast;



import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.placeprice.R;
import com.placeprice.uploadplace.DeletePlace;
import com.placeprice.uploadplace.IUpload;
import com.placeprice.uploadplace.UploaderPhoto;
import com.placeprice.uploadplace.UploaderPlace;
import com.placeprice.utils.GPSTracker;
import com.placeprice.utils.Place;
import com.placeprice.utils.PlaceImageSlidesFragmentAdapter;
import com.placeprice.utils.ScalingUtilities;
import com.placeprice.utils.ScalingUtilities.ScalingLogic;
import com.viewpagerindicator.CirclePageIndicator;

/**
 * Created by peri on 21/10/13.
 */
public class UploadPlaceActivity extends SherlockFragmentActivity implements IUpload{

    private Double latitude;
    private Double longitude;
    private String cityPlace = "ciudad";
    private String addressPlace = "direccion";
    private GPSTracker gps;
    private String idPlaceADD;
    private static final int TAKE_PICTURE = 1;
    private static final int GET_PICTURE = 2;
    private Uri output;
    private File imageFile1, imageFile2, imageFile3, imageFile4, imageFile5;
    private File imageFileRename;
    File filePhotoFolder;
    private boolean isSaveDataPlace = false;
    private String namePhoto;
    private Integer numphoto;
    private List<String> pathList;
    private List<Bitmap> bitList;
    int i;
    int dstWidth = 1200;
    int dstHeight = 900;
    PlaceImageSlidesFragmentAdapter mAdapter;
    ViewPager mPager;
    CirclePageIndicator mIndicator;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upload_place);
        gps = new GPSTracker(this);
        if(gps.canGetLocation()){
        	gps.getBestLocation();
        	gps.executeGeocoder(UploadPlaceActivity.this);
        }	
        numphoto = 0;
        i = 1;
        pathList = new ArrayList<String>();
        bitList = new ArrayList<Bitmap>();
        isWifiOn();
      /*  Button btnTakePhoto = (Button)findViewById(R.id.buttonAddPhoto);
        btnTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	if(numphoto < 5){
            		 namePhoto = "PIC_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".jpg";
                     filePhotoFolder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Placeprice");
                     if(!filePhotoFolder.exists()){
                         filePhotoFolder.mkdirs();
                     }

                     switch (numphoto){
                         case 0:
                             imageFile1 = new File(filePhotoFolder.getPath() + File.separator + namePhoto );
                             //fileList.add(imageFile1);
                             pathList.add(imageFile1.getPath());
                             output = Uri.fromFile(imageFile1);
                             break;
                         case 1:
                             imageFile2 = new File(filePhotoFolder.getPath() + File.separator + namePhoto);
                             //fileList.add(imageFile2);
                             pathList.add(imageFile2.getPath());
                             output = Uri.fromFile(imageFile2);
                             break;
                         case 2:
                             imageFile3 = new File(filePhotoFolder.getPath() + File.separator + namePhoto);
                             //fileList.add(imageFile3);
                             pathList.add(imageFile3.getPath());
                             output = Uri.fromFile(imageFile3);
                             break;
                         case 3:
                             imageFile4 = new File(filePhotoFolder.getPath() + File.separator + namePhoto);
                             //fileList.add(imageFile4);
                             pathList.add(imageFile4.getPath());
                             output = Uri.fromFile(imageFile4);
                             break;
                         case 4:
                             imageFile5 = new File(filePhotoFolder.getPath() + File.separator + namePhoto);
                            // fileList.add(imageFile5);
                             pathList.add(imageFile5.getPath());
                             output = Uri.fromFile(imageFile5);
                             break;
                     }
                     Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                     cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, output);
                     startActivityForResult(cameraIntent, TAKE_PICTURE);
            	}else{
            		Toast.makeText(getBaseContext(), "Ya has introducido 5 fotos", Toast.LENGTH_LONG).show();
            	}

            		
               
            }
        });
*/
        Button btnUp = (Button)findViewById(R.id.buttonPlaceUpload);
        btnUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText namePlace = (EditText)findViewById(R.id.namePlaceUpload);
                String name = namePlace.getText().toString();
                EditText cityEdit = (EditText)findViewById(R.id.cityPlaceUpload);
                String city = cityEdit.getText().toString();
                if( !name.isEmpty() && !city.isEmpty() && numphoto != 0){
                    if(isWifiOn()){
                    	if(gps.canGetLocation()){
                        	gps.getBestLocation();
                            latitude = gps.getLatitude();
                            longitude = gps.getLongitude();
                            showCheckAlert(name, city, numphoto, addressPlace).show();
                        }else{
                            gps.showSettingsAlert().show();
                        }
                    }
                }
                else{
                    Toast.makeText(getApplicationContext(),"Para a�adir un nuevo lugar tienes que obtener tu localizaci�n, introducir el nombre del lugar y al menos un foto de la carta con los precios del lugar", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.menu_upload, menu);
        return true;
      } 
    
    public boolean onOptionsItemSelected(MenuItem item) {
        
    	switch(item.getItemId()){
    		
    	case R.id.action_camera:
    		managerPhoto(false);
    		break;
    		
    	case R.id.action_location_upload:	
            if(isWifiOn()){
            	if(gps.canGetLocation()){
                	gps.getBestLocation();
                	gps.executeGeocoder(UploadPlaceActivity.this);
                	Toast.makeText(getApplicationContext(), "Obteniendo ubicaci�n...", Toast.LENGTH_SHORT).show();
                }else{
                    gps.showSettingsAlert().show();
                }
                
            }
            break;
    		
    		
    /*	case R.id.action_gallery:    	
    		Intent cameraIntent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);	
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, output);
            startActivityForResult(cameraIntent, GET_PICTURE);
    		break;*/
        }
       
        return true;
    }
    
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
  
    	if (requestCode == TAKE_PICTURE && resultCode == RESULT_OK) {
            numphoto++;
            bitList.clear();
            TextView textView = (TextView)findViewById(R.id.txtNumPhotos);
            textView.setText("Has a�adido hasta ahora " + numphoto.toString() + " de 5 fotos");
            for(String s : pathList){
            	bitList.add(ScalingUtilities.decodeFile(s, 300, 225,ScalingLogic.FIT));
            }
            
            mAdapter = new PlaceImageSlidesFragmentAdapter(getSupportFragmentManager(), bitList);

            mPager = (ViewPager)findViewById(R.id.pagerUpLoad);
            mPager.setAdapter(mAdapter);
            mIndicator = (CirclePageIndicator)findViewById(R.id.indicatorUpLoad);
            mIndicator.setViewPager(mPager);
        }else{
        	pathList.remove(pathList.size() - 1);
        }
    /*
     * if (requestCode == GET_PICTURE && resultCode == RESULT_OK && data != null){
    		Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(selectedImage,filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            pathList.add(picturePath);
    		bitList.clear();
    		numphoto++;
    		for(String s : pathList){
            	bitList.add(ScalingUtilities.decodeFile(s, 300, 225,ScalingLogic.FIT));
            }
    		TextView textView = (TextView)findViewById(R.id.txtNumPhotos);
            textView.setText("Has a�adido hasta ahora " + numphoto.toString() + " de 5 fotos");
    		mAdapter = new PlaceImageSlidesFragmentAdapter(getSupportFragmentManager(), bitList);

            mPager = (ViewPager)findViewById(R.id.pagerUpLoad);
            mPager.setAdapter(mAdapter);
            mIndicator = (CirclePageIndicator)findViewById(R.id.indicatorUpLoad);
            mIndicator.setViewPager(mPager);
    	}
    */	
    }

    public boolean savePlace(){
        boolean dataSuccess = false;
        EditText namePlace = (EditText)findViewById(R.id.namePlaceUpload);
        String namep = namePlace.getText().toString();
        //EditText cityPlace = (EditText)findViewById(R.id.cityPlaceUpload);
        //String cityp = cityPlace.getText().toString();
        Place place = new Place(namep,cityPlace.toLowerCase(),latitude, longitude, numphoto);
        UploaderPlace httpClient = new UploaderPlace(this);
        httpClient.addNameValuePlace(place);
        //UploaderPhoto uploaderPhoto = new UploaderPhoto(this);
        //File file = new File(photo);
        if(httpClient.isInternetAllowed(this)){
            httpClient.execute("http://www.placeprice.bugs3.com/servicephp/service.add3.places.php");
            dataSuccess = true;

        }else{
            Toast.makeText(getApplicationContext(),"Comprueba que tienes conexion a Internet",Toast.LENGTH_LONG).show();
        }
        return dataSuccess;
    }

    public void saveImagePlace(String result){
        i = 1;
        String[] imagPaths = new String[numphoto];;
        for(String path : pathList){
            imageFileRename = new File(filePhotoFolder.getPath()+ File.separator + "pic_" + result + "-" + i + ".jpg");
            String pathDecoded = decodeFile(path, imageFileRename.getAbsolutePath(), dstWidth, dstHeight);   
            imagPaths[i - 1] = pathDecoded;
            i++;
        }
        UploaderPhoto uploaderPhoto = new UploaderPhoto(this);
        uploaderPhoto.execute(imagPaths);
    }  

    public void managerPhoto(boolean is4Gallery){
    	if(numphoto < 5){
   		 namePhoto = "PIC_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".jpg";
            filePhotoFolder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Placeprice");
            if(!filePhotoFolder.exists()){
                filePhotoFolder.mkdirs();
            }

            switch (numphoto){
                case 0:
                    imageFile1 = new File(filePhotoFolder.getPath() + File.separator + namePhoto );
                    //fileList.add(imageFile1);
                    pathList.add(imageFile1.getPath());
                    output = Uri.fromFile(imageFile1);
                    break;
                case 1:
                    imageFile2 = new File(filePhotoFolder.getPath() + File.separator + namePhoto);
                    //fileList.add(imageFile2);
                    pathList.add(imageFile2.getPath());
                    output = Uri.fromFile(imageFile2);
                    break;
                case 2:
                    imageFile3 = new File(filePhotoFolder.getPath() + File.separator + namePhoto);
                    //fileList.add(imageFile3);
                    pathList.add(imageFile3.getPath());
                    output = Uri.fromFile(imageFile3);
                    break;
                case 3:
                    imageFile4 = new File(filePhotoFolder.getPath() + File.separator + namePhoto);
                    //fileList.add(imageFile4);
                    pathList.add(imageFile4.getPath());
                    output = Uri.fromFile(imageFile4);
                    break;
                case 4:
                    imageFile5 = new File(filePhotoFolder.getPath() + File.separator + namePhoto);
                   // fileList.add(imageFile5);
                    pathList.add(imageFile5.getPath());
                    output = Uri.fromFile(imageFile5);
                    break;
            }
            if(is4Gallery){
            	Intent cameraIntent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);	
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, output);
                startActivityForResult(cameraIntent, GET_PICTURE);
            }else{
            	Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, output);
                startActivityForResult(cameraIntent, TAKE_PICTURE);
            }
            
   	}else{
   		Toast.makeText(getBaseContext(), "Ya has introducido 5 fotos que es el m�ximo", Toast.LENGTH_LONG).show();
   	}

    }
    
    private String decodeFile(String path, String newPath,int DESIREDWIDTH, int DESIREDHEIGHT) {
        String strMyImagePath = null;
        Bitmap scaledBitmap = null;

        try {
            // Part 1: Decode image
            Bitmap unscaledBitmap = ScalingUtilities.decodeFile(path, DESIREDWIDTH, DESIREDHEIGHT, ScalingLogic.FIT);

            if (!(unscaledBitmap.getWidth() <= DESIREDWIDTH && unscaledBitmap.getHeight() <= DESIREDHEIGHT)) {
                // Part 2: Scale image
                scaledBitmap = ScalingUtilities.createScaledBitmap(unscaledBitmap, DESIREDWIDTH, DESIREDHEIGHT, ScalingLogic.FIT);
            } else {
                unscaledBitmap.recycle();
                return path;
            }

            // Store to tmp file
            
            File f = new File(newPath);
          
            strMyImagePath = f.getAbsolutePath();
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(f);
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 85, fos);
                fos.flush();
                fos.close();
            } catch (FileNotFoundException e) {

                e.printStackTrace();
            } catch (Exception e) {

                e.printStackTrace();
            }

            scaledBitmap.recycle();
        } catch (Throwable e) {
        }

        if (strMyImagePath == null) {
            return path;
        }
        return strMyImagePath;

    }
    
    public Boolean isWifiOn(){
	     WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
	     int state = wifi.getWifiState();
	     if(state == WifiManager.WIFI_STATE_DISABLED || state == WifiManager.WIFI_STATE_DISABLING){
	    	 showSettingsWiFiAlert().show();
	    	 return false;
	     }else
	    	 return true;
	 }
    
    public AlertDialog showCheckAlert(String name, String city, Integer np, String dir){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle("�Estas seguro de que deseas a�adir el lugar?");

        // Setting Dialog Message
        alertDialog.setMessage("Nombre: " + name + "\n" + 
        						"Direcci�n: " + dir +"\n" +
        						"Ciudad: " + city + "\n" +
        						"N�mero de fotos: " + np + "\n");
        // On pressing Settings button
        alertDialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
            	isSaveDataPlace = savePlace();
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        return alertDialog.create();
    }
    
    public AlertDialog showSettingsWiFiAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle("WiFi settings");

        // Setting Dialog Message
        alertDialog.setMessage("El WiFi no est� habilitado.\n" + "Si tienes simplemente el Wi-Fi encendido, tendr�s una mayor exactitud en tu localici�n.\n" + "�Quieres ir al menu de configuraci�n WiFi ?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Ir a ajustes de WiFi", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        return alertDialog.create();
    }
    
    public AlertDialog showDeleteAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle("�Estas seguro de que borrar esta foto?");

        
        // On pressing Settings button
        alertDialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
             
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        return alertDialog.create();
    }

    public void UpTaskCallback(String result) {
        if(result != null){
     	if(result.equals("fail")){
             Toast.makeText(getApplicationContext(),"No se ha podido a�adir el place, intentalo de nuevo o m�s tarde por favor", Toast.LENGTH_LONG).show();
         }
     	else{
     		idPlaceADD = result;
            // Toast.makeText(getApplicationContext(),"Se ha registrado el lugar con ID: "+result,Toast.LENGTH_LONG).show();
            // startActivity(new Intent(UploadPlaceActivity.this, MainActivity.class));
             if(isSaveDataPlace){    
             	Toast.makeText(getApplicationContext(),"Se han a�adido los datos del lugar con ID: "+result,Toast.LENGTH_LONG).show();
                 saveImagePlace(result);
             }

         }
        }else
     	   Toast.makeText(this, "Fallo en el servidor, no ha sido posible conectarse", Toast.LENGTH_LONG).show();
     }

     @Override
    public void UpTaskPhotoCallback(String result) {
     	DeletePlace deletePlace = new DeletePlace(this);
 		deletePlace.addNameValuePlace(idPlaceADD);
     	if(result!=null){
 	    	if(result.equals("fail")){
 	    		deletePlace.execute("http://www.placeprice.bugs3.com/servicephp/service.delete.place.php");
 	            Toast.makeText(getApplicationContext(),"No se ha podido a�adir la/s im�gen/es, por favor intentalo de nuevo o m�s tarde...", Toast.LENGTH_LONG).show();
 	        }
 	        if(result.equals("true")){
 	           if(pathList.size() == numphoto){
 	        	   gps.stopUsingGPS();
 	        	   Toast.makeText(getApplicationContext(), "Se han a�adido las imagenes del lugar con ID:" + idPlaceADD, Toast.LENGTH_LONG).show();
 	        	   startActivity(new Intent(UploadPlaceActivity.this, MainActivity.class));
 	        }
 	           }
         }else{
     		deletePlace.execute("http://www.placeprice.bugs3.com/servicephp/service.delete.place.php");
         	Toast.makeText(getApplicationContext(), "Ha habido un problema con el servidor, por favor intentalo de nuevo o m�s tarde...", Toast.LENGTH_LONG).show();
         }
     }
     
	@Override
	public void UpTaskDeleteCallback(String result) {
		if(result != null){
			if(result.equals("true")){
				Toast.makeText(getApplicationContext(), "Se han eliminado correctamente los datos del lugar", Toast.LENGTH_SHORT).show();
			}
			if(result.equals("fail")){
				Toast.makeText(getApplicationContext(), "No se ha podido eliminado el lugar", Toast.LENGTH_SHORT).show();
			}else{
				Toast.makeText(getApplicationContext(), "Ha ocurrido un error, probablemente sea el servidor...", Toast.LENGTH_SHORT).show();
			}
		}
		
	}

	@Override
	public void UpTaskAddressCallback(String result) {
		if(result != null){
			addressPlace = result;
			TextView add = (TextView)findViewById(R.id.txtAddress);
			add.setText("Direcci�n: " + addressPlace);
		}
	}

	@Override
	public void UpTaskCityCallback(String result) {
		if(result != null){
			cityPlace = result;
			EditText add = (EditText)findViewById(R.id.cityPlaceUpload);
			add.setText(cityPlace);
		}
		
		
	}
	
	
}



