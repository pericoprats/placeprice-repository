package com.placeprice.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); 
        setContentView(com.placeprice.R.layout.activity_main);
        Button btnSPlace = (Button)findViewById(com.placeprice.R.id.buttonMainPlace);
        btnSPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SearchPlaceActivity.class);
                startActivity(intent); 
            }
        });
        Button btnUpload = (Button)findViewById(com.placeprice.R.id.buttonMainUpload);
        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, UploadPlaceActivity.class);
                startActivity(intent);
            }
        });

        Button btnAround = (Button)findViewById(com.placeprice.R.id.buttonMainAround);
        btnAround.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AroundPlaceMainActivity.class);
                startActivity(intent);
            }
        });
        
        getActionBar().hide();

    }
    
}



